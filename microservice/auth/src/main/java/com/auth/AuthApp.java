package com.auth;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;

/**
 * *
 * 
 * @author phamhoanglinh
 */
@SpringBootApplication
@EnableDiscoveryClient // https://stackoverflow.com/questions/31976236/whats-the-difference-between-enableeurekaclient-and-enablediscoveryclient

public class AuthApp {

	public static final Logger logger = LoggerFactory.getLogger(AuthApp.class);

	public static void main(String[] arguments) throws Exception {
		SpringApplication.run(AuthApp.class, arguments);
	}

	/**
	 * 
	 * security configuration
	 *
	 */
	@Configuration
	@EnableWebSecurity
	@EnableGlobalMethodSecurity(prePostEnabled = true)
	@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
	public static class SecurityConfig extends WebSecurityConfigurerAdapter {

		/*
		 * there are 3 options: 
		 * - use custom Authentication Provider to do your own authentication. 
		 * - use custom UserDetailService to load by user name using default DaoAuthenticationProvider. 
		 * - use In Memory for testing below
		 */
		@Override
		protected void configure(AuthenticationManagerBuilder auth) throws Exception {
			auth.inMemoryAuthentication()
					.withUser("admin").password("pwd").roles("ADMIN")
				.and()
					.withUser("user").password("pwd").roles("USER");
		}

		@Override
		protected void configure(HttpSecurity http) throws Exception {
			http
					// .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
					.csrf().disable()
					.requestMatchers().antMatchers("/login", "/oauth/authorize")
					.and()
						.authorizeRequests().anyRequest().authenticated()
					.and()
						.formLogin().permitAll();
		}

		@Override
		public void configure(WebSecurity web) throws Exception {
			super.configure(web);
		}

		@Override
		@Bean
		public AuthenticationManager authenticationManagerBean() throws Exception {
			return super.authenticationManagerBean();
		}
	}

	/**
	 * 
	 * http://projects.spring.io/spring-security-oauth/docs/oauth2.html
	 * 
	 * oauth2 security configuration
	 *
	 */
	@Configuration
	@EnableAuthorizationServer
	public static class OAuth2SecurityConfig extends AuthorizationServerConfigurerAdapter {

		@Autowired
		private Environment env;

		/*
		 * use the same authenticationManager with Security above ( default is
		 * ProviderManager)
		 */
		@Autowired
		@Qualifier("authenticationManagerBean")
		private AuthenticationManager authenticationManager;

		/*
		 * defines the security constraints on the token endpoint
		 * 
		 */
		@Override
		public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
			security
					// .tokenEndpointAuthenticationFilters(filters); //add more filters to token endpoints before BasicAuthenticationFilter.
					.tokenKeyAccess("permitAll()")
					.checkTokenAccess("isAuthenticated()");
		}

		/*
		 * A configurer that defines the client details service. Client details
		 * can be initialized, or you can just refer to an existing store.
		 * 
		 * (a callback from your AuthorizationServerConfigurer) can be used to
		 * define an in-memory or JDBC implementation of the client details
		 * service. Important attributes of a client are :clientId, secret,
		 * scope, authorizedGrantTypes, authorities
		 * 
		 * http://www.baeldung.com/spring-security-oauth-dynamic-client-registration
		 */
		@Override
		public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
			clients.inMemory()
					.withClient("app1")
					.secret("app1secret")
					.authorizedGrantTypes("authorization_code")
					.scopes("user_info")
					.autoApprove(true)
				.and()
					.withClient("browser")
					.accessTokenValiditySeconds(10 * 60) // 10 minutes
					.refreshTokenValiditySeconds(24 * 60 * 60) // 24 hours
					.authorizedGrantTypes("refresh_token", "password")
					.scopes("ui")
				.and()
					.withClient("account-service")
					.secret("123").accessTokenValiditySeconds(10 * 60) // 10  minutes
					.refreshTokenValiditySeconds(24 * 60 * 60) // 24 hours
					.authorizedGrantTypes("client_credentials", "refresh_token").scopes("server");
		}

		/*
		 * defines the authorization and token endpoints and the token services.
		 * 
		 */
		@Override
		public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
			final TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
			tokenEnhancerChain.setTokenEnhancers(Arrays.asList(tokenEnhancer(), accessTokenConverter()));

			endpoints
					// .tokenServices(tokenServices()) //enable access token in UUID format
					.tokenStore(tokenStore()).tokenEnhancer(tokenEnhancerChain)
					.authenticationManager(authenticationManager);

		}

		/*
		 * add additional info into access token payload.
		 */
		@Bean
		public TokenEnhancer tokenEnhancer() {
			return new CustomToken();
		}

		// enable access token in UUID format
		// @Bean
		// @Primary
		// public DefaultTokenServices tokenServices() { //
		// DefaultTokenServices defaultTokenServices = new
		// DefaultTokenServices(); //
		// defaultTokenServices.setTokenStore(tokenStore()); //
		// defaultTokenServices.setSupportRefreshToken(true);
		// return
		// defaultTokenServices;
		// }

		/*
		 * override in memory store InMemoryTokenStore, using JWT.
		 */
		@Bean
		public TokenStore tokenStore() {
			return new JwtTokenStore(accessTokenConverter());
		}

		/*
		 * enable JWT function to generate JWT token using asymmetric(public/private key) or symmetric key.
		 * It allow to customize token payload by setAccessTokenConverter function, default is DefaultAccessTokenConverter
		 */
		@Bean
		public JwtAccessTokenConverter accessTokenConverter() {
			JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
			
			KeyStoreKeyFactory keyStoreKeyFactory = new KeyStoreKeyFactory(
					new ClassPathResource(env.getProperty("token.file.path")), env.getProperty("token.file.password").toCharArray());
			
			converter.setKeyPair(keyStoreKeyFactory.getKeyPair(env.getProperty("token.file.keypair")));
			
			// converter.setAccessTokenConverter(tokenConverter); //customize token payload by setAccessTokenConverter function
			return converter;

		}
	}

	@Configuration
	@EnableResourceServer
	public static class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

		private static final String RESOURCE_ID = "my_rest_api";

		@Override
		public void configure(ResourceServerSecurityConfigurer resources) {
			resources.resourceId(RESOURCE_ID).stateless(false);
		}

		@Override
		public void configure(HttpSecurity http) throws Exception {
			http.requestMatchers().antMatchers("/user/**")
			.and()
				.authorizeRequests().anyRequest().authenticated()
			.and()
				.exceptionHandling().accessDeniedHandler(new OAuth2AccessDeniedHandler());
		}

	}
}